class EdgeNode:
    def set_type(self, type):
        self.__type = type

    def get_type(self):
        return self.__type

    def set_disk_size(self, disk_size):
        temp = disk_size.get('greater_or_equal')
        self.__disk_size = temp

    def get_disk_size(self):
        return self.__disk_size

    def set_num_cpu(self, cpu):
        temp = cpu.get('greater_or_equal')
        self.__cpu = temp

    def get_num_cpu(self):
        return self.__cpu

    def set_mem_size(self, mem_size):
        temp = mem_size.get('greater_or_equal')
        self.__mem_size = temp

    def get_mem_size(self):
        return self.__mem_size

    def set_gpu_model(self, model):
        self.__model = model

    def get_gpu_model(self):
        return self.__model

    def set_gpu_dedicated(self, dedicated):
        self.__dedicated = dedicated

    def get_gpu_dedicated(self):
        return self.__dedicated

    def set_ip(self, ip):
        self.__ip = ip

    def get_ip(self):
        return self.__ip

    def set_region(self, region):
        self.__region = region

    def get_region(self):
        return self.__region

    def set_device(self, device):
        self.__device = device

    def get_device(self):
        return self.__device

    def set_conditionRegisteredDevices(self, condition):
        self.__condition = condition

    def get_conditionRegisteredDevices(self):
        return self.__condition

    def set_registered(self, registered):
        self.__registered = registered

    def get_registered(self):
        return self.__registered

    def set_endDevice(self, end_device):
        self.__end_device = end_device

    def get_endDevice(self):
        return self.__end_device

    def set_ask(self, ask):
        self.__ask = ask

    def get_ask(self):
        return self.__ask

    def set_forgetMobile(self, mobile):
        self.__mobile = mobile

    def get_forgetMobile(self):
        return self.__mobile

    def set_node(self, node):
        self.__node = node

    def get_node(self):
        return self.__node

    def set_relationship(self, relationship):
        self.__relationship = relationship

    def get_relationship(self):
        return self.__relationship

    def set_ping(self, ping):
        self.__ping = ping

    def get_ping(self):
        return self.__ping

    def set_spawn(self, spawn):
        self.__spawn = spawn

    def get_spawn(self):
        return self.__spawn