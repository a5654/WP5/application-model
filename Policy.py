class Policy:
    def set_min_instance(self, min_instance):
        self.__min_instance = min_instance

    def get_min_instance(self):
        return self.__min_instance

    def set_max_instance(self, max_instance):
        self.__max_instance = max_instance

    def get_max_instance(self):
        return self.__max_instance

    def set_default_instance(self, default_instance):
        self.__default_instance = default_instance

    def get_default_instance(self):
        return self.__default_instance

    def set_increment(self, increment):
        self.__increment = increment

    def get_increment(self):
        return self.__increment

    def set_target(self, target):
        self.__target = target

    def get_target(self):
        return self.__target
