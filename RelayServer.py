class RelayServer:

    def set_edge_ip(self, edge_ip):
        self.__edge_ip = edge_ip


    def get_edge_ip(self):
        return self.__edge_ip

    def set_type(self, type):
        self.__type = type

    def get_type(self):
        return self.__type

    def set_relationship(self, connection):
        self.__connection = connection

    def get_relationship(self):
        return self.__connection

    def set_sessions(self, sessions):
        self.__sessions = sessions

    def get_sessions(self):
        return self.__sessions