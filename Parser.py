import os

import AdminTool
import Container
import yaml

import Converter
import EdgeNode
import EndDevice
import Cloud
import Policy
import GameServer
import LobbyServer
import User
import RelayServer


def ReadFile(app, scenario, path):
    userlist = []
    edgenodelist = []
    with open(path) as file:
        # The FullLoader parameter handles the conversion from YAML
        # scalar values to Python the dictionary format
        file = yaml.load(file, Loader=yaml.FullLoader)
        topology = file.get('topology_template')
        node_template = topology.get('node_templates')
        node_names = node_template.keys()
        for x in node_names:
            node = node_template.get(x)
            print(node)
            type = node.get('type')
            if 'container' in type:
                container = Container.Container()
                container.set_type(type)
                if 'properties' in node:
                    properties = node.get('properties')
                    if 'lobbiesIP' in properties:
                        lobbyip = properties.get('lobbiesIP')
                        container.set_lobbiesIP(lobbyip)
                if app == 'ORBK' and scenario == '1':
                    dockerImage = properties.get('dockerImage')
                    container.set_dockerImage(dockerImage)
                if 'requirements' in node:
                    requirements = node.get('requirements')
                    container.set_relatioship(requirements)
                    if 'node' in requirements:
                        host = node.get('node')
                        container.set_host(host)
            if 'EdgeNode' in type:
                edge = EdgeNode.EdgeNode()
                edge.set_type(type)
                if 'requirements' in node:
                    relationship = node.get('requirements')
                    edge.set_relationship(relationship)
                if 'properties' in node:
                    properties = node.get('properties')
                    ip = properties.get('edgeNodeIP')
                    edge.set_ip(ip)
                    if 'node_filter' in node:
                        node_filter = node.get('node_filter')
                        capabilities = node_filter.get('capabilities')
                        properties = capabilities[0].get('properties')
                        cpu = properties.get('num_cpus')
                        ram = properties.get('mem_size')
                        disk = properties.get('disk_size')
                        gpu = properties.get('gpu_model')
                        gpu_name = gpu.get('model')
                        gpu_dedication = gpu.get('dedicated')
                        edge.set_num_cpu(cpu)
                        edge.set_mem_size(ram)
                        edge.set_disk_size(disk)
                        edge.set_gpu_model(gpu_name)
                        edge.set_gpu_dedicated(gpu_dedication)
                    if app == 'PLEXUS' and scenario == '2':
                        if 'actions' in properties:
                            actions = properties.get('actions')
                            # find mobile
                            if 'findMobile' in actions:
                                find = actions.get('findMobile')
                                findinfo = find.get('properties')
                                device = findinfo.get('device')
                                edge.set_device(device)
                                location = end_device.get_location()
                                edge.set_region(location)
                                forget = findinfo.get('forgetMobile')
                                edge.set_forgetMobile(forget)
                            # check
                            if 'check' in actions:
                                check = actions.get('check')
                                checkinfo = check.get('properties')
                                condition = checkinfo.get('conditionRegisteredDevices')
                                spawn = checkinfo.get('spawn')
                                edge.set_conditionRegisteredDevices(condition)
                                edge.set_spawn(spawn)
                            # isTheEndDeviceRegistered
                            if 'isTheEndDeviceRegistered' in actions:
                                question = properties.get('isTheEndDeviceRegistered')
                                registered = question.get('registered')
                                device = edge.get_device()
                                ask = question.get('ask')
                                edge.set_registered(registered)
                                edge.set_ask(ask)
                                edge.set_endDevice(device)
                                edgenodelist.append(edge)
                    if app == 'PLEXUS' and scenario == '3':
                        actions = properties.get('actions')
                        check = actions.get('check')
                        checkinfo = check.get('properties')
                        condition = checkinfo.get('conditionRegisteredDevices')
                        ping = checkinfo.get('ping')
                        edge.set_ping(ping)
                        edge.set_conditionRegisteredDevices(condition)
            if app == 'OVR':
                if 'RelayServer' in type:
                    properties = node.get('properties')
                    relay_server = RelayServer.RelayServer()
                    if scenario == '1':
                        relay_server.set_type(type)
                        edgeNodeIP = properties.get('edgeNodeIP')
                        get_property = edgeNodeIP.get('get_property')
                        if 'EdgeNode' in get_property:
                            relay_server.set_edge_ip(edge.get_ip())
                    if scenario == '2':
                        activesessions = properties.get('activeSessions')
                        relay_server.set_sessions(activesessions)
                        if ('user' in properties) and userlist:
                            for user in userlist:
                                userIP = user.get_ip()
                                userID = user.get_userid()
                                userLocation = user.get_userlocation()
            if 'Cloud' in type:
                cloud = Cloud.Cloud()
                cloud.set_type(type)
                if 'properties' in node:
                    properties = node.get('properties')
                    if app == 'PLEXUS' and scenario == '2':
                        actions = properties.get('actions')
                        forget = actions.get('forget')
                        forgetinfo = forget.get('properties')
                        tell = forgetinfo.get('tell')
                        forget_node = forgetinfo.get('forget')
                        cloud.set_tell(tell)
                        cloud.set_forget(forget_node)
                        registereddevices = properties.get('registeredDevices')
                        number = properties.get('numberOfRegisteredDevices')
                        cloud.set_registeredDevices(registereddevices)
                        cloud.set_numberofRegisteredDevices(number)
                    if app == 'ORBK' and scenario == '2':
                        ip = properties.get('ip')
                        gameserverip = properties.get('gameserverip')
                        gameserverport = properties.get('gameserverport')
                        cloud.set_gameserverIP(gameserverip)
                        cloud.set_lobbyserverIP(ip)
                        cloud.set_gameserverPort(gameserverport)
                    if 'actions' in properties:
                        actions = properties.get('actions')
                        if 'deploy' in actions:
                            deploy = actions.get('deploy')
                            deploy_info = deploy.get('properties')
                            image = deploy_info.get('image')
                            cloud.set_image(image)
                            if 'deploy condition' in deploy_info:
                                deploy_condition = deploy_info.get('deploy condition')
                                cloud.set_deploy_condition(deploy_condition)
                        if 'shutdown' in actions:
                            shutdown = actions.get('shutdown')
                            shut_info = shutdown.get('properties')
                            image = shut_info.get('image')
                            cloud.set_image(image)
                        if 'update' in actions:
                            update = actions.get('update')
                            update_info = update.get('properties')
                            update_condition = update_info.get('update condition')
                            dockerImage = update_info.get('image')
                            cloud.set_image(dockerImage)
                            cloud.set_update_condition(update_condition)
                        if 'terminate' in actions:
                            terminate = actions.get('terminate')
                            terminate_info = terminate.get('properties')
                            update_c = terminate_info.get('update condition')
                            deploy_c = terminate_info.get('deploy condition')
                            shutdown = terminate_info.get('shutdown')
                            new = terminate_info.get('new')
                            cloud.set_terminate_update_condition(update_c)
                            cloud.set_terminate_deploy_condition(deploy_c)
                            cloud.set_newserver(new)
                            cloud.set_shutdown(shutdown)
                        if 'upload' in actions:
                            upload = actions.get('upload')
                            upload_info = upload.get('properties')
                            lobby_image = upload_info.get('imageLobby')
                            game_image = upload_info.get('imageGame')
                            cloud.set_game_image(game_image)
                            cloud.set_lobby_image(lobby_image)
                if 'requirement' in node:
                    requirements = node.get('requirements')
                    cloud.set_relationship(requirements)
            if 'EndDevice' in type:
                end_device = EndDevice.EndDevice()
                if 'properties' in node:
                    properties = node.get('properties')
                    if app == 'OVR' and scenario == '1':
                        edgeIP = properties.get('edgeNodeIP')
                        get_property = edgeNodeIP.get('get_property')
                        if 'Cloud' in get_property:
                            end_device.set_edge_ip(cloud.get_edge_ip())
                        actions = properties.get('actions')
                        start = actions.get('start')
                        end_device.set_start_application(start)
                    if 'User' in properties:
                        user = User.User()
                        if 'User1' in properties:
                            userinfo = properties.get('User1')
                        if 'User2' in properties:
                            userinfo = properties.get('User2')
                        else:
                            userinfo = properties.get('User')
                        user_properties = userinfo.get('properties')
                        userIP = user_properties.get('userIP')
                        userID = user_properties.get('userID')
                        userLocation = user_properties.get('userLocation')
                        user.set_ip(userIP)
                        user.set_userid(userID)
                        user.set_userlocation(userLocation)
                        if app == 'OVR' and scenario == '2':
                            actions = properties.get('actions')
                            userlist.append(user)
                            active = actions.get('actions')
                            end_device.set_activeSessions(active)
                    if app == 'ORBK' and scenario == '2':
                        actions = properties.get('actions')
                        requestLobbyIP = actions.get('requestLobbyIP')
                        requestLobbyIP_info = requestLobbyIP.get('properties')
                        lobbyip = requestLobbyIP_info.get('lobbyIP')
                        end_device.set_lobbyip(lobbyip)
                        requestGameInit = actions.get('requestGameInitialize')
                        requestGameInit_info = requestGameInit.get('properties')
                        gameinit = requestGameInit_info.get('gameInitialize')
                        end_device.set_gameinit(gameinit)
                    location = properties.get('location')
                    end_device.set_location(location)
                if 'requirements' in node:
                    requirements = node.get('requirements')
                    end_device.set_relationship(requirements)
            if app == 'PLEXUS' and scenario == '3':
                if 'policies' in type:
                    policy = Policy.Policy()
                    properties = node.get('properties')
                    min_instances = properties.get('min_instances')
                    max_instances = properties.get('max_instances')
                    default_instances = properties.get('default_properties')
                    increment = properties.get('increment')
                    targets = node.get('targets')
                    policy.set_target(targets)
                    policy.set_increment(increment)
                    policy.set_default_instance(default_instances)
                    policy.set_max_instance(max_instances)
                    policy.set_min_instance(min_instances)
            if app == 'ORBK':
                if 'AdminTool' in type:
                    tool = AdminTool.AdminTool()
                    properties = node.get('properties')
                    if scenario == '6' or scenario == '7':
                        dockerImage = properties.get('dockerImage')
                        tool.set_dockerImage(dockerImage)
                    if 'actions' in properties:
                        actions = properties.get('actions')
                        if 'create' in actions:
                            create = actions.get('create')
                            create_info = create.get('properties')
                            create_condition = create_info.get('create condition')
                            lobbyserver = create_info.get('target')
                            tool.set_create_condition(create_condition)
                            tool.set_lobbyserver(lobbyserver)
                        if actions == 'select':
                            select = actions.get('select')
                            select_info = select.get('properties')
                            select_condition = select_info.get('select_condition')
                            lobbyserver = select_info.get('target')
                            tool.set_select_condition(select_condition)
                            tool.set_lobbyserver(lobbyserver)
                        if actions == 'selectRegion':
                            select_region = actions.get('selectRegion')
                            select_region_info = select_region.get('properties')
                            region_condition = select_region_info.get('region condition')
                            region = select_region_info.get('Region')
                            gameserver = select_region_info.get('GameServer')
                        if 'check' in actions:
                            check = actions.get('check')
                            checkinfo = check.get('properties')
                            condition = checkinfo.get('conditionRegion')
                            if 'deactivateButton' in checkinfo:
                                deactivate = checkinfo.get('deactivateButton')
                                tool.set_deactivateButton(deactivate)
                            if 'activateButton' in checkinfo:
                                activateButton = checkinfo.get('activateButton')
                                tool.set_activateButton(activateButton)
                            tool.set_condition(condition)
                    if scenario == '8':
                        lobby_image = properties.get('LobbyImage')
                        game_image = properties.get('GameImage')
                        tool.set_lobby_image(lobby_image)
                        tool.set_game_image(game_image)
                    requirements = node.get('requirements')
                    tool.set_relationship(requirements)
                if 'GameServer' in type:
                    gameserver = GameServer.GameServer()
                    properties = node.get('properties')
                    image = properties.get('dockerImage')
                    gameserver.set_dockerImage(image)
                    if scenario != '7':
                        ip = properties.get('ip')
                        port = properties.get('port')
                        gameserver.set_gameserverIP(ip)
                        gameserver.set_gameserverPort(port)
                    if 'actions' in properties:
                        actions = properties.get('actions')
                        if 'check' in actions:
                            check = actions.get('check')
                            checkinfo = check.get('properties')
                            number = checkinfo.get('number')
                            operator = checkinfo.get('operator')
                            time = checkinfo.get('time')
                            gameserver.set_number(number)
                            gameserver.set_times(time)
                    if 'region' in properties:
                        region = properties.get('region')
                        gameserver.set_region(region)
                    requirements = node.get('requirements')
                    gameserver.set_relationship(requirements)
                if 'LobbyServer' in type:
                    lobbyserver = LobbyServer.LobbyServer()
                    properties = node.get('properties')
                    if scenario == '2':
                        actions = properties.get('actions')
                        ip = properties.get('ip')
                        check = actions.get('check')
                        checkinfo = check.get('properties')
                        condition = checkinfo.get('conditionNumberofPlayers')
                        lobbyserver.set_number(condition)
                        send = actions.get('send')
                        sendinfo = send.get('properties')
                        host = sendinfo.get('node')
                        gameserverip = sendinfo.get('GameServerIP')
                        gameserverport = sendinfo.get('GameServerPort')
                        lobbyserver.set_gameserverIP(gameserverip)
                        lobbyserver.set_gameserverPort(gameserverport)
                        lobbyserver.set_host(host)
                        lobbyserver.set_ip(ip)
                    if scenario == '4' or scenario == '6':
                        dockerImage = properties.get('dockerImage')
                        lobbyserver.set_image(dockerImage)
                        if 'state' in properties:
                            state = properties.get('state')
                            lobbyserver.set_state(state)
                        requirements = node.get('requirements')
                        lobbyserver.set_relationship(requirements)
        # Converter.tosca_to_k8s(app, scenario, container, edge, cloud, end_device)
