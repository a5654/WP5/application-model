class AdminTool:
    def set_create_condition(self, create_condition):
        self.__create_condition = create_condition

    def get_create_condition(self):
        return self.__create_condition

    def set_update_condition(self, update_condition):
        self.__update_condition = update_condition

    def get_update_condition(self):
        return self.__update_condition

    def set_relationship(self, connection):
        self.__connection = connection

    def get_relationship(self):
        return self.__connection

    def set_deactivateButton(self,dbutton):
        self.__dbutton = dbutton

    def get_deactivateButton(self):
        return self.__dbutton

    def set_activateButton(self, abutton):
        self.__abutton = abutton

    def get_activateButton(self):
        return self.__abutton

    def set_dockerImage(self, image):
        self.__image = image

    def get_dockerImage(self):
        return self.__image

    def set_lobbyserver(self,lobby):
        self.__lobby = lobby

    def get_lobbyserver(self):
        return self.__lobby

    def set_gameserver(self, game):
        self.__game = game

    def get_gameserver(self):
        return self.__game

    def set_region(self,region):
        self.__region = region

    def get_region(self):
        return self.__region

    def set_select_condition(self, select_condition):
        self.__select_condition = select_condition

    def get_select_condition(self):
        return self.__select_condition

    def set_lobby_image(self, lobby_image):
        self.__lobby_image = lobby_image

    def get_lobby_image(self):
        return self.__lobby_image

    def set_game_image(self, game_image):
        self.__game_image = game_image

    def get_game_image(self):
        return self.__game_image

    def set_condition(self, condition):
        self.__condition = condition

    def get_condition(self):
        return self.__condition