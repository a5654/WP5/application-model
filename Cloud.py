class Cloud:
    def set_type(self, type):
        self.__type = type

    def get_type(self):
        return self.__type

    def set_relationship(self, connection):
        self.__connection = connection

    def get_relationship(self):
        return self.__connection


    def set_image(self, image):
        self.__image = image

    def get_image(self):
        return self.__image

    def set_lobbyserverIP(self, lobbyIP):
        self.__lobbyIP = lobbyIP

    def get_lobbyserverIP(self):
        return self.__lobbyIP

    def set_gameserverIP(self, gameIP):
        self.__gameIP = gameIP

    def get_gameserverIP(self):
        return self.__gameIP

    def set_gameserverPort(self, gamePort):
        self.__gamePort = gamePort

    def get_gameserverPort(self):
        return self.__gamePort

    def set_update_condition(self, update):
        self.__update = update

    def get_update_condition(self):
        return self.__update

    def set_deploy_condition(self, deploy):
        self.__deploy = deploy

    def get_deploy_condition(self):
        return self.__deploy

    def set_lobbyserver(self, lobby):
        self.__lobby = lobby

    def get_lobbyserver(self):
        return self.__lobby

    def set_newserver(self, new):
        self.__new = new

    def get_newserver(self):
        return self.__new

    def set_tell(self, tell):
        self.__tell = tell

    def get_tell(self):
        return self.__tell

    def set_forget(self, forget):
        self.__forget = forget

    def get_forget(self):
        return self.__forget

    def set_numberofRegisteredDevices(self, number):
        self.__number = number

    def get_numberofRegisteredDevices(self):
        return self.__number

    def set_registeredDevices(self, regdevices):
        self.__regdevice = regdevices

    def get_registeredDevices(self):
        return self.__regdevice

    def set_edge_ip(self, edge_ip):
        self.__edge_ip = edge_ip

    def get_edge_ip(self):
        return self.__edge_ip

    def set_terminate_update_condition(self, terminate_u_condition):
        self.__terminate_u_condition = terminate_u_condition

    def get_terminate_update_condition(self):
        return self.__terminate_u_condition

    def set_terminate_deploy_condition(self, terminate_d_condition):
        self.__terminate_d_condition = terminate_d_condition

    def get_terminate_deploy_condition(self):
        return self.__terminate_d_condition

    def set_shutdown(self,shutdown):
        self.__shutdown = shutdown

    def get_shutdown(self):
        return self.__shutdown

    def set_lobby_image(self,lobby_image):
        self.__lobby_image = lobby_image

    def get_lobby_image(self):
        return self.__lobby_image

    def set_game_image(self,game_image):
        self.__game_image = game_image

    def get_game_image(self):
        return self.__game_image