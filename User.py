class User:
    def set_ip(self, ip):
        self.__ip = ip

    def get_ip(self):
        return self.__ip

    def set_userid(self, id):
        self.__id = id

    def get_userid(self):
        return self.__id

    def set_userlocation(self, location):
        self.__location = location

    def get_userlocation(self):
        return self.__location