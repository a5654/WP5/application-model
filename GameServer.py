class GameServer:

    def set_relationship(self, connection):
        self.__connection = connection

    def get_relationship(self):
        return self.__connection

    def set_gameserverIP(self, gameIP):
        self.__gameIP = gameIP


    def get_gameserverIP(self):
        return self.__gameIP


    def set_gameserverPort(self, gamePort):
        self.__gamePort = gamePort


    def get_gameserverPort(self):
        return self.__gamePort

    def set_dockerImage(self, image):
        self.__image = image

    def get_dockerImage(self):
        return self.__image

    def set_times(self, tries):
        self.__tries = tries

    def get_times(self):
        return self.__tries

    def set_number(self, number):
        self.__number = number

    def get_number(self):
        return self.__number

    def set_region(self, region):
        self.__region = region

    def get_region(self):
        return self.__region