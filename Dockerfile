# set base image (host OS)
FROM debian:stable

# set the working directory in the container

WORKDIR /application-model
ADD . /application-model/

# install dependencies
RUN apt-get update && apt-get install -y \
python3-pip

RUN pip3 install \
oyaml \
flask \
flask-restful \
tosca-parser



EXPOSE 4000
# command to run on container start
CMD [ "python3", "/application-model/WebService.py" ]
