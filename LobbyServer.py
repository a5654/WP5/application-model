class LobbyServer:

    def set_number(self, number):
        self.__number = number

    def get_number(self):
        return self.__number

    def set_host(self, host):
        self.__host = host

    def get_host(self):
        return self.__host

    def set_gameserverIP(self, gameIP):
        self.__gameIP = gameIP

    def get_gameserverIP(self):
        return self.__gameIP

    def set_gameserverPort(self, gamePort):
        self.__gamePort = gamePort

    def get_gameserverPort(self):
        return self.__gamePort

    def set_relationship(self, connection):
        self.__connection = connection

    def get_relationship(self):
        return self.__connection

    def set_ip(self, ip):
        self.__ip = ip

    def get_ip(self):
        return self.__ip

    def set_image(self, image):
        self.__image = image

    def get_image(self):
        return self.__image

    def set_state(self, state):
        self.__state = state

    def get_state(self):
        return self.__state
