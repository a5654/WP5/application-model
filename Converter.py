import oyaml as yaml
import EdgeNode


def tosca_to_k8s(app, scenario, container, edge, cloud, end_device):
    if app == 'OVR' and scenario == '1':
        pod = {
            'apiVersion': 'extensions/v1beta1',
            'kind': 'Pod',
            'metadata': {'name': app + '-POD'},
            'spec': {
                'hostNetwork': True,
                'containers': [{'name': container.get_type(), 'image': 'image',
                                'resources': {'limits': {'cpu': edge.get_num_cpu(),
                                                         'memory': edge.get_mem_size()},
                                              'requests': {
                                                  'storage': edge.get_disk_size()}}}]}}
        with open('kubernetes/' + app + 'scenario' + scenario + 'POD' + '.yml', 'w') as outfile:
            yaml.dump(pod, outfile, default_flow_style=False)
        service = {'apiVersion': 'v1',
                   'kind': 'Service',
                   'metadata': {'name': container.get_type(), 'labels': {'app': app}},
                   'spec': {'ports': [{'port': 3306}], 'selector': {'app': app, 'tier': 'backend'},
                            'clusterIP': 'None'}}
        with open('kubernetes/' + app + 'scenario' + scenario + 'Service' + '.yml', 'w') as outfile:
            yaml.dump(service, outfile, default_flow_style=False)
        pv = {'apiVersion': 'v1',
              'kind': 'PersistentVolumeClaim',
              'metadata': {'name': app + '-pv-claim', 'labels': {'app': app}},
              'spec': {'accessMode': ['ReadWriteOnce'], 'resources': {'requests': {'storage': edge.get_disk_size()}}}}
        with open('kubernetes/' + app + 'scenario' + scenario + 'PV' + '.yml', 'w') as outfile:
            yaml.dump(pv, outfile, default_flow_style=False)
        deployment = {'apiVersion': 'apps/v1',
                      'kind': 'Deployment',
                      'metadata': {'name': app, 'labels': {'app': app}},
                      'spec': {'selector': {'matchLabels': {'app': app, 'tier': 'backend'}},
                               'strategy': {'type': 'Recreate'},
                               'template': {'metadata': {'labels': {'app': app, 'tier': 'backend'}},
                                            'spec': {'containers': [
                                                {'image': 'image', 'name': app, 'ports': [{'containerPort': 3306,'name':app}]}]}}}}
        with open('kubernetes/' + app + 'scenario' + scenario + 'Deployment' + '.yml', 'w') as outfile:
            yaml.dump(deployment, outfile, default_flow_style=False)
