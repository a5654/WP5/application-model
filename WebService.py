import os

from flask_restful import Resource, Api
from flask import Flask, send_file, render_template, jsonify, request
import Parser
import TOSCAValidator
import ValidMessage

app = Flask(__name__)
api = Api(app)
home = str(os.getcwd())


@app.errorhandler(404)
def page_not_found(error):
    app.logger.error('Page not found: %s', request.path)
    return render_template('404.html'), 404


app.register_error_handler(404, page_not_found)


def error_page(message):
    filename = 'error_page.html'
    f = open(home + "/templates/" + filename, 'w')
    html = """<!DOCTYPE html>\n<html lang="en">\n    <body>\n       <h1> """ + message + """ </h1>\n    </body>\n</html>"""
    f.write(html)
    f.close()


def isValid(path):
    validator = TOSCAValidator.TopologyValidator()
    v = validator.validate(path)
    valid = ValidMessage.printValidation(v)
    print(valid)
    if valid:
        return True
    else:
        return False


def cases(path):
    isValid(path)
    #if valid:
        #Parser.ReadFile(application, scenario, path)


@app.route('/app_model', methods=['GET'])
def characterization():
    application = request.args.get('app')
    scenario = request.args.get('scenario')
    if application and scenario:
        path = home + "/yaml-examples/" + application + "/" + application.lower() + "_tosca_scenario" + scenario + ".yml"
        cases(path)
    if not application or not scenario:
        error_page(" Please give both application and scenario parameters ")
        return render_template("error_page.html")
    try:
        return send_file(path, as_attachment=True)
    except Exception as e:
        error_page(application + " does not have scenario " + scenario)
        print(e)
        return render_template("error_page.html")


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=4000)
