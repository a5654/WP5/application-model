class EndDevice:
    def set_type(self, type):
        self.__type = type

    def get_type(self):
        return self.__type

    def set_start_application(self, start):
        self.__start = start

    def get_start_application(self):
        return self.__start

    def set_relationship(self, connection):
        self.__connection = connection

    def get_relationship(self):
        return self.__connection

    def set_edge_ip(self, edge_ip):
        self.__edge_ip = edge_ip

    def get_edge_ip(self):
        return self.__edge_ip

    def set_activeSessions(self, sessions):
        self.__sessions = sessions

    def get_activeSessions(self):
        return self.__sessions

    def set_location(self, location):
        self.__location = location

    def get_location(self):
        return self.__location

    def set_player(self, player):
        self.__location = player

    def get_location(self):
        return self.__location

    def set_lobbyip(self,lobbyip):
        self.__lobbyip = lobbyip

    def get_lobbyip(self):
        return self.__lobbyip

    def set_gameinit(self,condition):
        self.__condition = condition

    def get_gemeinit(self):
        return  self.__condition